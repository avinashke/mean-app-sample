var express = require('express');
var cookieParser = require('cookie-parser');
var flash=require("connect-flash");
var cookieSession = require('cookie-session');
var passport = require('passport');
var bodyparser = require('body-parser');
var morgan = require('morgan');
var validate_user_post = require('./api/validate_user_post')
var helpers = require('./helpers').helpers;
var port = process.env.PORT || 5000;
var cors = require('cors') 
var app = express()
app.use(cors())
//body parser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));


app.use(morgan('dev'));
app.use(cookieParser());
app.use(flash());

//cookie-session
app.use(
    cookieSession({
      maxAge: 30 * 24 * 60 * 60 * 1000,
      keys: ['manoj']
    })
);

// Passport init
app.use(passport.initialize());
app.use(passport.session());


app.use('/api',validate_user_post);
require('./router').establishRoutes(app, helpers);

app.listen(port)
console.log('port number 5000 is running')