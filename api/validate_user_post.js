var express = require('express');
  var passport = require('passport');
  var LocalStrategy = require('passport-local').Strategy;
  var bcrypt = require('bcrypt');
  var helpers = require('../helpers').helpers
  var getDbclient = require('../authentic')
  var Managers = require('../data_access/managers').Managers;
  var app = express()
  var router = express.Router();
  var email = ''

  
  passport.serializeUser(function(user, done) {
     console.log('Serialize user:'+JSON.stringify(user));
      done(null, user);
  });
    
  passport.deserializeUser(function(user, done) {
      console.log("Deserialize called....",user);
      done(null, user);
  });




  passport.use(new LocalStrategy(
      function(username, password, done) {
          email = username
          getDbclient(function(err, db) {

              if (err) { return done(err); }

              db.collection('managers').find({ username: username }).toArray().then((user) => {

                  if (user.length === 0) {
                      console.log('user not find');
                      return done(null, false, { message: 'Incorrect username.' });

                  }
                  var hash = bcrypt.compareSync(password, user[0].password);
                  if (hash == false) {
                      console.log('password not matched');
                      return done(null, false, { message: 'Incorrect password.' });
                  }
                  return done(null, user[0]);
              }, (err) => {
                  throw err;
              });
              db.close();
          });
      }));



  router.post('/login',
      passport.authenticate('local', {
          successRedirect: '/api/success',
          failureRedirect: '/api/failure',
          session: true,
          failureFlash: true
      })
  );

  router.get('/success', (req, res) => {
      Managers.updateManagers({ "username": email }, { $set: {has_access: true } }, helpers, function(response) {

      })
      res.status(200).send('login successful');

  });

  router.get('/failure', (req, res) => {
      res.status(401).send('Invalid');
  });

  router.get('/logout', function(req, res) {
      Managers.updateManagers({ "username": email }, { $set: {has_access: false } }, helpers, function(response) {

      })
   if(req.user==undefined||req.user==""){
    res.send("No Logged in user found , please 'log in' first....")
   }else{
    req.logout();
    res.send("logouts")
}
})
   
  module.exports = router;