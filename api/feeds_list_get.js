(function() {
    function execute(req, res, next, helpers) {
        var feed = require('../data_access/feeds').Feeds;
        var brand = require('../data_access/brands').Brands;
        var manager = require('../data_access/managers').Managers;

        var id = req.params.id
        var ObjectID = require('mongodb').ObjectID;
        var objId = new ObjectID(id);
        var Access = req.params.access
        if (Access == 'true') {
            Access = true
            manager.getManagersList({ "brand_id": objId, "has_access": Access }, {}, helpers, function(managers) {
                if (managers == "") {

                    res.send("Alert: Manager is not logged in..\n---------------")
                } else {

                    feed.getFeeds({ "brand_id": objId }, {}, helpers, function(feeds) {

                        if (feeds == "") {
                            res.send("Alert : feeds not found of this Brand..\n---------------------------------------")

                        } else {
                            brand.getBrands({ _id: objId }, {}, helpers, function(brands) {
                                var idToName = brands[0].brand_name;
                                for (var i = 0; i < feeds.length; i++) {
                                    feeds[i].brand_id = idToName
                                }
                                res.send(feeds)
                            })

                        }
                    })
                }
            })
        } else if (Access == 'false') {
            Access = false

            manager.getManagersList({ "brand_id": objId, "has_access": Access }, {}, helpers, function(managers) {

                if (managers == "") {

                    res.send("Alert: Not found Manager of this Brand..\n---------------------------------------")
                } else {

                    res.send({ No_access_Permission: managers })
                }
            })
        }
    }

    exports.execute = execute
})()